const http = require("http");
const port = 4000;

http
  .createServer(function (request, response) {
    if (request.url == "/" && request.method == "GET") {
      response.writeHead(200, { "Content-Type": "text/plain" });
      response.write("Welcome to booking system");
      response.end();
    }

    if (request.url == "/profile" && request.method == "GET") {
      response.writeHead(200, { "Content-Type": "text/plain" });
      response.write("Welcome to your profile!");
      response.end();
    }

    if (request.url == "/courses" && request.method == "GET") {
      response.writeHead(200, { "Content-Type": "text/plain" });
      response.write("Here’s our courses available");
      response.end();
    }
    if (request.url == "/add-course" && request.method == "POST") {
      response.writeHead(200, { "Content-Type": "text/plain" });
      response.write("Add a course to our resources");
      response.end();
    }
    if (request.url == "/update-course" && request.method == "PUT") {
      response.writeHead(200, { "Content-Type": "text/plain" });
      response.write("Update a course to our resources");
      response.end();
    }
    if (request.url == "/archive-courses" && request.method == "DELETE") {
      response.writeHead(200, { "Content-Type": "text/plain" });
      response.write("Archive courses to our resources");
      response.end();
    }
  })
  .listen(port);

console.log(`Server running at localhost: ${port}`);

/* Create a simple server and the following routes with their
corresponding HTTP methods and responses:
a. If the url is http://localhost:4000/, send a response Welcome to Booking System
-get
b. If the url is http://localhost:4000/profile, send a response Welcome to your
profile!  -get
c. If the url is http://localhost:4000/courses, send a response Here’s our courses
available -get
d. If the url is http://localhost:4000/addcourse, send a response Add a course to our
resources -post

Activity

Copyright@2019 Tuitt, Inc. and its affiliates. Confidential 10
3. Create a simple server and the following routes with their
corresponding HTTP methods and responses:

e. If the url is http://localhost:4000/updatecourse, send a response Update a
course to our resources -put
f. If the url is http://localhost:4000/archivecourses, send a response Archive
courses to our resources -delete
Test all the endpoints in Postman.
4. Create a git repository named S32.
5. Initialize a local git repository, add the remote link and push to git with
the commit message of Add s32 activity code.
6. Add the link in Boodle.
*/

/*


let http = require("http");

// mock database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
];

http.createServer(function (request, response) {
	// GET Method
	if( request.url == "/users" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'application/json'});
		// JSON.stringify()
		response.write(JSON.stringify(directory));
		response.end();
	};

	// POST Method
	if (request.url == "/addUser" && request.method == "POST"){

		// Declare and initialize a "requestBody" variable to an empty string
		let requestBody = "";

		// "request.on" - used to handle incoming data in a HTP server
		// Data is received from the client and is processed in the "data" stream
		request.on('data', function (data) {
			// Adds and assigns the data retrieved from the data stream to requestBody
			requestBody += data;
		});

		// only runs after the request has been completely sent
		request.on('end', function() {
			// converts string requestBody to JSON
			requestBody = JSON.parse(requestBody);

			// Create a new object representing the new mock database record
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}

			// Add the new user into the method database
			directory.push(newUser);

			response.writeHead(200, {'Content-Type': 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		});
	};


}).listen(3000);

console.log("Server running at localhost: 3000");


*/
